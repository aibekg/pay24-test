module.exports = {
  presets: ['module:metro-react-native-babel-preset'],
  plugins: [
    [
      'module-resolver',
      {
        root: ['.'],
        alias: {
          '@components': './src/components',
          '@routes': './src/routes',
          '@icons': './src/assets/icons',
          '@store': './src/store',
          '@screens': './src/screens',
          '@utils': './src/utils',
          '@api': './src/api',
          '@hooks': './src/hooks',
          '@navigations': './src/navigations',
          '@features': './src/features',
          '@src': './src',
          '@services': './src/services',
        },
      },
    ],
    [
      'module:react-native-dotenv',
      {
        envName: 'APP_ENV',
        moduleName: '@env',
        path: '.env',
      },
    ],
  ],
};
