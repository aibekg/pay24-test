import React from 'react';
import {createNativeStackNavigator} from '@react-navigation/native-stack';

import {APP_STACK_SCREENS} from '@routes/index';
import {ERootStackRoutes} from '@routes/types';

const Stack = createNativeStackNavigator();

export const screenOptions = {
  headerShown: false,
};

export const NavigationStack = () => {
  return (
    <Stack.Navigator
      screenOptions={screenOptions}
      initialRouteName={ERootStackRoutes.Home}>
      {APP_STACK_SCREENS.map(screen => (
        <Stack.Screen
          key={screen.name}
          name={screen.name}
          component={screen.component}
        />
      ))}
    </Stack.Navigator>
  );
};
