import {CountryScreen} from '@screens/Country';
import {HomeScreen} from '@screens/Home';

import {ERootStackRoutes} from './types';

export const APP_STACK_SCREENS = [
  {name: ERootStackRoutes.Home, component: HomeScreen},
  {name: ERootStackRoutes.Country, component: CountryScreen},
];
