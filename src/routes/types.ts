import {RouteProp} from '@react-navigation/native';
import {NativeStackNavigationProp} from '@react-navigation/native-stack';

export enum ERootStackRoutes {
  Home = 'Home',
  Country = 'Country',
}

export type TRootStackParamList = {
  [ERootStackRoutes.Home]: undefined;
  [ERootStackRoutes.Country]: undefined;
};

export type TNavigationProp<RouteName extends keyof TRootStackParamList> =
  NativeStackNavigationProp<TRootStackParamList, RouteName>;

export type TNavigationRouteProp<RouteName extends keyof TRootStackParamList> =
  RouteProp<TRootStackParamList, RouteName>;
