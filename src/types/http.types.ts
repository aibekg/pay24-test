export type HttpHeader = {
  Authorization?: string;
  'Content-Type'?: string;
  accept?: string;
};
