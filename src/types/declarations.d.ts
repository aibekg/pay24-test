declare module '@env' {
  export const BASE_URL: string;
  export const ENCRYPTION_KEY: string;
}

declare module '*.svg' {
  import React from 'react';
  import {SvgProps} from 'react-native-svg';
  const content: React.FC<SvgProps>;
  export default content;
}
