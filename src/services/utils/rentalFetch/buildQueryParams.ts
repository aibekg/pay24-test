import {Data} from '@services/utils/rentalFetch/rentalFetch.types';
import {stringify} from 'query-string';

export const buildQueryParams = ({queryParams}: Data) => {
  return queryParams
    ? `?${stringify(queryParams, {arrayFormat: 'comma'})}`
    : '';
};
