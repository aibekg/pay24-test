export const handleResponse = (response: Response) => {
  if (response.status >= 200 && response.status < 300) {
    return response;
  }

  throw Error(`${response.url} responded with status ${response.status}`);
};
