import {rentalFetch} from '@services/utils/rentalFetch/rentalFetch';

import {Data} from './rentalFetch.types';
import {withLogger} from './withLogger';

export const rentalFetchUnauthorized = async (path: string, data: Data) => {
  return await withLogger(rentalFetch, path, data);
};
