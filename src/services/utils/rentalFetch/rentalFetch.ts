import {BASE_URL} from '@env';
import {Data} from '@services/utils/rentalFetch/rentalFetch.types';

import {buildBody} from './buildBody';
import {buildHeaders} from './buildHeaders';
import {buildQueryParams} from './buildQueryParams';
import {handleResponse} from './handleResponse';

export const rentalFetch = async (path: string, data: Data) => {
  const queryParams = buildQueryParams(data);
  const url = `${BASE_URL}/${path}${queryParams}`;

  const response = await fetch(url, {
    method: data?.method || 'GET',
    headers: buildHeaders(data),
    body: buildBody(data),
    signal: data.signal,
  });
  return handleResponse(response);
};
