import {BASE_URL} from '@env';
import {Data, RequestFunc} from '@services/utils/rentalFetch/rentalFetch.types';

import {buildQueryParams} from './buildQueryParams';

export const withLogger = async (
  func: RequestFunc,
  path: string,
  data: Data,
) => {
  let isFinished = false;
  const queryParams = buildQueryParams(data);
  const url = `${BASE_URL}/${path}${queryParams}`;

  console.info(url);
  data.signal?.addEventListener('abort', () => {
    if (isFinished) {
      return;
    }

    console.info('<<Aborted>>', url);
  });

  const result = await func(path, data);
  isFinished = true;
  return result;
};
