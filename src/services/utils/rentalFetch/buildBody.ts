import {Data} from '@services/utils/rentalFetch/rentalFetch.types';

export const buildBody = ({body}: Data) => {
  if (body) {
    return JSON.stringify(body);
  }
};
