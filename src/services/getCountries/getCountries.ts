import {rentalFetchUnauthorized} from '@services/utils/rentalFetch';

import {Country} from '@store/countries';
import {RegionType} from '@store/regions';

export type GetCountriesBYRegionProps = {
  region: RegionType;
};

export type GetCountriesByName = {
  countryName: string;
};

const countryFields = 'name,capital,population,area,ccn3,languages,flags';

export const getAllCountries = async (): Promise<Country[]> => {
  const response = await rentalFetchUnauthorized('all/', {
    queryParams: {fields: countryFields},
  });
  return await response.json();
};

export const getCountriesByRegion = async ({
  region,
}: GetCountriesBYRegionProps): Promise<Country[]> => {
  const response = await rentalFetchUnauthorized(`region/${region}/`, {
    queryParams: {fields: countryFields},
  });
  return await response.json();
};

export const getCountriesByName = async ({
  countryName,
}: GetCountriesByName): Promise<Country[]> => {
  const response = await rentalFetchUnauthorized(`name/${countryName}/`, {
    queryParams: {fields: countryFields},
  });
  return await response.json();
};
