import React from 'react';
import {Home} from '@features/Home/Home';

export const HomeScreen = () => {
  return <Home />;
};
