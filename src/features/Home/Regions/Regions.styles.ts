import {StyleSheet} from 'react-native';

export const styles = StyleSheet.create({
  content: {
    flexGrow: 1,
    height: 60,
    flexDirection: 'row',
    alignItems: 'center',
    paddingBottom: 12,
  },
});
