import React, {useCallback} from 'react';
import {ScrollView} from 'react-native';
import {useSelector} from 'react-redux';
import {Region} from '@features/Home/Region';

import {useActions} from '@hooks/useActions';
import {Region as TRegion, selectActiveRegion} from '@store/regions';

import {regions} from './Regions.mock';
import {styles} from './Regions.styles';

export const Regions = () => {
  const activeRegion = useSelector(selectActiveRegion);
  const {setActiveRegion} = useActions();

  const handleRegionPress = useCallback(
    (region: TRegion) => {
      setActiveRegion({region});
    },
    [setActiveRegion],
  );

  return (
    <ScrollView
      contentContainerStyle={styles.content}
      horizontal={true}
      showsHorizontalScrollIndicator={false}>
      {regions.map(region => (
        <Region
          onPress={handleRegionPress}
          active={region.id === activeRegion.id}
          key={region.id}
          region={region}
        />
      ))}
    </ScrollView>
  );
};
