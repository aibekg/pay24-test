import {Region} from '@store/regions/types';

export const regions: Region[] = [
  {
    id: 1,
    label: 'all',
  },
  {
    id: 2,
    label: 'europe',
  },
  {
    id: 3,
    label: 'africa',
  },
  {
    id: 4,
    label: 'americas',
  },
  {
    id: 5,
    label: 'asia',
  },
];
