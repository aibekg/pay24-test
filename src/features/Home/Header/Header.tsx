import React from 'react';
import {View} from 'react-native';

import {Regions} from '../Regions';

import {styles} from './Header.styles';

export const Header = () => {
  return (
    <View style={styles.container}>
      <Regions />
    </View>
  );
};
