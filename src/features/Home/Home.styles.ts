import {StyleSheet} from 'react-native';

import {colors, sizes} from '@utils/theme';

export const styles = StyleSheet.create({
  container: {
    padding: sizes.box.S,
    flex: 1,
  },
  favorite: {
    position: 'absolute',
    right: sizes.box.XXL,
    bottom: sizes.box.XXL,
    width: 50,
    height: 50,
    borderRadius: sizes.border.circle,
    backgroundColor: colors.commons.white,
    borderColor: colors.commons.background,
    shadowColor: colors.commons.red,
    shadowOffset: {
      width: 2,
      height: 2,
    },
    shadowOpacity: 0.1,
    shadowRadius: 12,
    elevation: 4,
  },
});
