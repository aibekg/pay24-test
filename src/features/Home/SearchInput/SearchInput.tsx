import React, {useEffect, useRef, useState} from 'react';
import {View} from 'react-native';

import {Icon} from '@components/Icon';
import {Input} from '@components/Input';
import {useActions} from '@hooks/useActions';

import {styles} from './SearchInput.styles';

export const SearchInput = () => {
  const {fetchCountriesByName, setActiveRegion, fetchCountriesByRegion} =
    useActions();
  const [text, setText] = useState('');
  const timerRef = useRef<number | null>(null);

  useEffect(() => {
    if (timerRef.current != null) {
      clearInterval(timerRef.current);
    }

    timerRef.current = setTimeout(() => {
      fetchCountries();
    }, 1000);

    return () => {
      if (timerRef.current != null) {
        clearInterval(timerRef.current);
      }
    };
  }, [text]);

  const fetchCountries = () => {
    if (text) {
      setActiveRegion({region: {label: 'all', id: 1}});
      fetchCountriesByName({name: text.trim()});
      return;
    }
    fetchCountriesByRegion({region: 'all'});
  };

  return (
    <View style={styles.container}>
      <Input
        value={text}
        onChangeText={setText}
        prefix={<Icon name="search" />}
      />
    </View>
  );
};
