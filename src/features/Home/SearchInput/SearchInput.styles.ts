import {StyleSheet} from 'react-native';

import {colors, sizes} from '@utils/theme';

export const styles = StyleSheet.create({
  container: {
    paddingBottom: sizes.box.S,
    backgroundColor: colors.commons.background,
  },
});
