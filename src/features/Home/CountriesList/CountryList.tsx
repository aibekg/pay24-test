import React, {useCallback, useMemo} from 'react';
import {FlatList, RefreshControl, Text, View} from 'react-native';
import {useSelector} from 'react-redux';
import {useNavigation} from '@react-navigation/native';

import {CountryWidget} from '@components/CountryWidget';
import {Icon} from '@components/Icon';
import {useActions} from '@hooks/useActions';
import {ERootStackRoutes, TNavigationProp} from '@routes/types';
import {
  Country,
  selectCountries,
  selectFavoritesCountries,
} from '@store/countries';

import {SearchInput} from '../SearchInput/SearchInput';

import {styles} from './CountriesList.styles';

export const CountriesList = () => {
  const navigation = useNavigation<TNavigationProp<ERootStackRoutes.Home>>();
  const {data, fetching} = useSelector(selectCountries);
  const favoritesCountries = useSelector(selectFavoritesCountries);
  const {addFavoriteCountry, removeFavoriteCountry} = useActions();

  const ListEmptyComponent = useMemo(
    () => (
      <View style={styles.empty}>
        <Icon name="queue-list" />
        <Text style={styles.emptyText}>
          {"We couldn't find a country \n with that name"}
        </Text>
      </View>
    ),
    [],
  );

  const handleFavoritePress = useCallback(
    (country: Country) => {
      const foundItem = favoritesCountries.find(
        item => item?.ccn3 === country?.ccn3,
      );

      if (foundItem) {
        removeFavoriteCountry({countryCode: foundItem?.ccn3});
        return;
      }

      addFavoriteCountry({country});
    },
    [addFavoriteCountry, favoritesCountries, removeFavoriteCountry],
  );

  const handleCountryPress = useCallback(() => {
    navigation.navigate(ERootStackRoutes.Country);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const renderItem = useCallback(
    (country: Country) => {
      const foundItem = favoritesCountries.find(
        item => item?.ccn3 === country?.ccn3,
      );

      return (
        <CountryWidget
          isFavorite={!!foundItem}
          onPress={handleCountryPress}
          onFavoritePress={handleFavoritePress}
          country={country}
        />
      );
    },
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [favoritesCountries, handleFavoritePress],
  );

  return (
    <FlatList<Country>
      data={data}
      stickyHeaderIndices={[0]}
      showsVerticalScrollIndicator={false}
      keyExtractor={country => country.ccn3}
      renderItem={({item}) => renderItem(item)}
      refreshControl={
        <RefreshControl refreshing={fetching} tintColor="#2A2A2A" />
      }
      ListHeaderComponent={<SearchInput />}
      ListEmptyComponent={ListEmptyComponent}
    />
  );
};
