import {StyleSheet} from 'react-native';

import {deviceHeight} from '@utils/deviceHelpers';

export const styles = StyleSheet.create({
  empty: {
    justifyContent: 'center',
    alignItems: 'center',
    height: deviceHeight - 240,
  },
  emptyText: {
    marginTop: 16,
    color: '#646C7A',
    fontSize: 16,
    fontWeight: '400',
    opacity: 0.5,
    textAlign: 'center',
  },
});
