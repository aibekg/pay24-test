import {StyleSheet} from 'react-native';

import {colors, sizes} from '@utils/theme';

export const styles = StyleSheet.create({
  container: {
    paddingHorizontal: sizes.box.S,
    paddingVertical: sizes.box.XS,
    borderRadius: sizes.border.XS,
    backgroundColor: colors.commons['black-70'],
    marginLeft: sizes.box.S,
    justifyContent: 'center',
    alignItems: 'center',
  },
  active: {
    opacity: 0.7,
  },
  label: {
    color: colors.text.white,
    textTransform: 'capitalize',
    fontWeight: '600',
  },
});
