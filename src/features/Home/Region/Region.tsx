import React, {useCallback} from 'react';
import {Text, TouchableOpacity} from 'react-native';

import {Region as TRegion} from '@store/regions';

import {styles} from './Region.styles';

export type RegionProps = {
  region: TRegion;
  active?: boolean;
  onPress: (region: TRegion) => void;
};

export const Region = ({region, active, onPress}: RegionProps) => {
  const handlePress = useCallback(() => {
    onPress(region);
  }, [region, onPress]);
  return (
    <TouchableOpacity
      disabled={active}
      onPress={handlePress}
      style={[styles.container, active && styles.active]}>
      <Text style={styles.label}>{region.label}</Text>
    </TouchableOpacity>
  );
};
