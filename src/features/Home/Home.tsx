import React, {useEffect} from 'react';
import {Keyboard, TouchableOpacity} from 'react-native';
import {useSelector} from 'react-redux';

import {Button} from '@components/Button';
import {useActions} from '@hooks/useActions';
import {selectActiveRegion} from '@store/regions';

import {CountriesList} from './CountriesList';
import {Header} from './Header';
import {styles} from './Home.styles';

export const Home = () => {
  const {fetchCountriesByRegion} = useActions();
  const activeRegion = useSelector(selectActiveRegion);

  useEffect(() => {
    fetchCountriesByRegion({region: activeRegion.label});
  }, [activeRegion.label]);

  return (
    <TouchableOpacity
      activeOpacity={1}
      onPress={Keyboard.dismiss}
      style={styles.container}>
      <Header />
      <CountriesList />
      <Button
        icon="favorite"
        iconColor="red"
        iconWidth={30}
        iconHeight={30}
        buttonStyles={styles.favorite}
      />
    </TouchableOpacity>
  );
};
