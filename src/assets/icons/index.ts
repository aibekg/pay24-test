import FavoriteIcon from '@icons/favorite.svg';
import QueueListIcon from '@icons/queue-list.svg';
import SearchIcon from '@icons/search.svg';
import SettingAdjustIcon from '@icons/settings-adjust.svg';

export const icons = {
  search: SearchIcon,
  favorite: FavoriteIcon,
  'queue-list': QueueListIcon,
  'setting-adjust': SettingAdjustIcon,
};
