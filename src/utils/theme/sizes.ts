export const sizes = {
  text: {
    XXS: 4,
    XS: 8,
    S: 12,
    M: 16,
    L: 20,
    XL: 22,
    XXL: 24,
  },
  box: {
    XXS: 4,
    XS: 8,
    S: 12,
    M: 16,
    L: 20,
    XL: 22,
    XXL: 24,
  },
  height: {
    input: 52,
  },
  border: {
    XXXS: 1,
    XXS: 4,
    XS: 8,
    S: 12,
    M: 16,
    L: 20,
    XL: 22,
    XXL: 24,
    circle: 100,
  },
};
