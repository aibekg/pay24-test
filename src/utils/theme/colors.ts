export const colors = {
  commons: {
    background: '#F4F4F4',
    white: '#FFFFFF',
    main: '#37B77C',
    'black-70': '#212121',
    black: '#000000',
    red: '#480000',
  },
  text: {
    main: 'rgba(0,0,0,0.87)',
    placeholder: 'rgba(33,33,33,0.56)',
    white: '#FFFFFF',
  },
};
