import React from 'react';
import {
  StyleProp,
  Text,
  TextStyle,
  TouchableOpacity,
  ViewStyle,
} from 'react-native';

import {Icon} from '@components/Icon';
import {icons} from '@icons/index';
import {sizes} from '@utils/theme';

import {styles} from './Button.styles';

export type ButtonProps = {
  buttonStyles?: StyleProp<ViewStyle>;
  textStyles?: StyleProp<TextStyle>;
  disabled?: boolean;
  text?: string;
  icon?: keyof typeof icons;
  iconColor?: string;
  iconWidth?: number;
  iconHeight?: number;
  onPress?: () => void;
};

export const Button = ({
  buttonStyles,
  textStyles,
  disabled,
  text,
  icon,
  iconColor,
  iconWidth = 20,
  iconHeight = 20,
  onPress,
}: ButtonProps) => {
  return (
    <TouchableOpacity
      activeOpacity={0.7}
      onPress={onPress}
      disabled={disabled}
      style={[styles.container, buttonStyles]}>
      {icon && (
        <Icon
          name={icon}
          width={iconWidth}
          height={iconHeight}
          fill={iconColor}
          style={{marginRight: text ? sizes.box.XS : 0}}
        />
      )}
      {text && <Text style={[styles.text, textStyles]}>{text}</Text>}
    </TouchableOpacity>
  );
};
