import {StyleSheet} from 'react-native';

import {sizes} from '@utils/theme';

export const styles = StyleSheet.create({
  container: {
    padding: sizes.box.XS,
    borderWidth: 0.5,
    borderRadius: sizes.border.S,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  text: {
    fontSize: sizes.text.M,
    fontWeight: '500',
  },
});
