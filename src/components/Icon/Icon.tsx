import React from 'react';
import {SvgProps} from 'react-native-svg';

import {icons} from '@icons/index';

type Props = SvgProps & {
  name: keyof typeof icons;
};

export const Icon = ({name, fill = 'black', ...svgProps}: Props) => {
  const SvgIcon = icons[name];

  return <SvgIcon {...svgProps} title={name} fill={fill} />;
};
