import React from 'react';
import {Image, Text, TouchableOpacity, View} from 'react-native';

import {Button} from '@components/Button';
import {Country} from '@store/countries';

import {styles} from './CountryWidget.styles';

export type CountryWidgetProps = {
  country: Country;
  onFavoritePress: (country: Country) => void;
  onPress: () => void;
  isFavorite: boolean;
};

export const CountryWidget = ({
  country,
  onFavoritePress,
  isFavorite,
  onPress,
}: CountryWidgetProps) => {
  return (
    <TouchableOpacity
      onPress={onPress}
      style={styles.container}
      activeOpacity={0.7}>
      <View style={styles.leftBlock}>
        <Image
          style={styles.flag}
          source={{uri: country.flags.png}}
          resizeMode="contain"
        />
        <Button
          buttonStyles={styles.favoriteButton}
          textStyles={styles.favoriteText}
          iconColor={isFavorite ? 'red' : '#FFFFFF'}
          icon="favorite"
          text="Favorite"
          onPress={() => onFavoritePress(country)}
        />
      </View>
      <View style={styles.rightBLock}>
        <Text style={styles.name}>{country.name.common}</Text>
        <Text>Capital: {country.capital}</Text>
        <Text>Population: {country.population}</Text>
        <Text>Area: {country.area}</Text>
        <Text>Country Code: {country.ccn3}</Text>
      </View>
    </TouchableOpacity>
  );
};
