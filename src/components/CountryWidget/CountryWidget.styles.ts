import {StyleSheet} from 'react-native';

import {colors, sizes} from '@utils/theme';

export const styles = StyleSheet.create({
  container: {
    padding: sizes.box.S,
    borderRadius: sizes.border.S,
    backgroundColor: colors.commons.white,
    marginVertical: sizes.box.XS,
    flexDirection: 'row',
  },
  leftBlock: {
    justifyContent: 'center',
  },
  rightBLock: {
    marginLeft: sizes.box.S,
  },
  flag: {
    width: 100,
    height: 60,
  },
  favoriteButton: {
    borderRadius: sizes.border.XXS,
    marginTop: sizes.box.S,
    backgroundColor: colors.commons['black-70'],
    width: 100,
  },
  favoriteText: {
    color: colors.text.white,
    fontSize: 14,
  },
  name: {
    fontSize: sizes.text.M,
    fontWeight: '600',
    color: colors.text.main,
    marginBottom: sizes.box.XXS,
  },
});
