import {StyleSheet} from 'react-native';

import {colors, sizes} from '@utils/theme';

export const styles = StyleSheet.create({
  container: {
    backgroundColor: colors.commons.white,
    paddingHorizontal: sizes.box.S,
    borderRadius: sizes.border.S,
    height: sizes.height.input,
    flexDirection: 'row',
  },
  prefix: {
    flex: 0.1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  input: {
    paddingHorizontal: sizes.box.S,
    flex: 1,
  },
});
