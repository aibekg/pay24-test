import React, {ReactNode} from 'react';
import {
  StyleProp,
  TextInput,
  TextInputProps,
  TextStyle,
  View,
  ViewStyle,
} from 'react-native';

import {styles} from './Input.styles';

export type CustomInputProps = {
  prefix?: ReactNode;
  containerStyles?: StyleProp<ViewStyle>;
  inputStyles?: StyleProp<TextStyle>;
};

export type InputProps = CustomInputProps & TextInputProps;

export const Input = ({
  prefix,
  containerStyles,
  inputStyles,
  placeholder = 'Search',
  ...restProps
}: InputProps) => {
  return (
    <View style={[styles.container, containerStyles]}>
      {prefix && <View style={styles.prefix}>{prefix}</View>}
      <TextInput
        style={[styles.input, inputStyles]}
        {...restProps}
        placeholder={placeholder}
      />
    </View>
  );
};
