import {
  getAllCountries,
  getCountriesByName,
  getCountriesByRegion,
} from '@services/getCountries';
import {put, takeLatest} from 'redux-saga/effects';

import {
  fetchCountriesByNameSuccess,
  fetchCountriesByRegionSuccess,
} from '@store/countries/actions';

import {
  Country,
  ECountriesAction,
  FetchCountriesByNameReturnType,
  FetchCountriesByRegionReturnType,
} from './types';

export function* fetchCountriesWorker({
  payload,
}: FetchCountriesByRegionReturnType) {
  try {
    let countries: Country[] = [];

    if (payload.region === 'all') {
      countries = yield getAllCountries();

      yield put(fetchCountriesByRegionSuccess({countries}));
      return;
    }

    countries = yield getCountriesByRegion({
      region: payload.region,
    });

    yield put(fetchCountriesByRegionSuccess({countries}));
  } catch (e) {
    yield put({type: ECountriesAction.FETCH_ERROR});
  }
}

export function* fetchCountriesByName({
  payload,
}: FetchCountriesByNameReturnType) {
  try {
    const countries: Country[] = yield getCountriesByName({
      countryName: payload.name,
    });

    yield put(fetchCountriesByNameSuccess({countries}));
  } catch (e) {
    yield put({type: ECountriesAction.FETCH_ERROR});
  }
}

export function* countriesSaga() {
  yield takeLatest(ECountriesAction.FETCH_BY_NAME, fetchCountriesByName);
  yield takeLatest(ECountriesAction.FETCH_BY_REGION, fetchCountriesWorker);
}
