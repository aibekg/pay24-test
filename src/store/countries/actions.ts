import {
  AddFavoriteCountryProps,
  AddFavoriteCountryReturnType,
  ECountriesAction,
  FetchCountriesByNameProps,
  FetchCountriesByNameSuccessProps,
  FetchCountriesByRegionProps,
  FetchCountriesByRegionSuccessProps,
  RemoveFavoriteCountryProps,
  RemoveFavoriteCountryReturnType,
} from './types';

export const fetchCountriesByRegion = (
  payload: FetchCountriesByRegionProps,
) => ({
  type: ECountriesAction.FETCH_BY_REGION,
  payload,
});

export const fetchCountriesByRegionSuccess = (
  payload: FetchCountriesByRegionSuccessProps,
) => ({
  type: ECountriesAction.FETCH_BY_REGION_SUCCESS,
  payload,
});

export const fetchCountriesByName = (payload: FetchCountriesByNameProps) => ({
  type: ECountriesAction.FETCH_BY_NAME,
  payload,
});

export const fetchCountriesByNameSuccess = (
  payload: FetchCountriesByNameSuccessProps,
) => ({
  type: ECountriesAction.FETCH_BY_NAME_SUCCESS,
  payload,
});

export const addFavoriteCountry = (
  payload: AddFavoriteCountryProps,
): AddFavoriteCountryReturnType => ({
  type: ECountriesAction.ADD_FAVORITE,
  payload,
});

export const removeFavoriteCountry = (
  payload: RemoveFavoriteCountryProps,
): RemoveFavoriteCountryReturnType => ({
  type: ECountriesAction.REMOVE_FAVORITE,
  payload,
});
