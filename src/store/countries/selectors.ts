import {createSelector} from 'reselect';

import {countriesReducer} from '@store/countries/reducer';
import {RootState} from '@store/reducers';

type CountriesState = ReturnType<typeof countriesReducer>;

const selectedState = (state: RootState): CountriesState => state.countries;

export const selectCountries = createSelector(
  selectedState,
  (state: CountriesState) => state.countries,
);

export const selectFavoritesCountries = createSelector(
  selectedState,
  (state: CountriesState) => state.favoritesCountries,
);
