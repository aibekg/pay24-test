import {AsyncData} from '@store/consts';
import {RegionType} from '@store/regions';

export type CountriesState = {
  countries: AsyncData<Country[]>;
  favoritesCountries: Country[];
};

export type Country = {
  name: Name;
  ccn3: string;
  capital: string[];
  languages: Languages;
  area: number;
  population: number;
  flags: Flag;
};

export type Flag = {
  png: string;
  svg: string;
  alt: string;
};

export type Name = {
  common: string;
  official: string;
  nativeName: NativeName;
};

export type NativeName = Record<string, NativeNameItem>;

export type NativeNameItem = {
  official: string;
  common: string;
};

export type Languages = {
  eng: string;
};

export enum ECountriesAction {
  FETCH_BY_REGION = 'FETCH_BY_REGION',
  FETCH_BY_REGION_SUCCESS = 'FETCH_BY_REGION_SUCCESS',
  FETCH_BY_NAME = 'FETCH_BY_NAME',
  FETCH_BY_NAME_SUCCESS = 'FETCH_BY_NAME_SUCCESS',
  FETCH_ERROR = 'FETCH_ERROR',
  ADD_FAVORITE = 'ADD_FAVORITE',
  REMOVE_FAVORITE = 'REMOVE_FAVORITE',
}

export type AddFavoriteCountryProps = {
  country: Country;
};

export type RemoveFavoriteCountryProps = {
  countryCode: string;
};

export type FetchCountriesByRegionProps = {
  region: RegionType;
};

export type FetchCountriesByNameProps = {
  name: string;
};

export type FetchCountriesByNameSuccessProps = {
  countries: Country[];
};

export type FetchCountriesByRegionSuccessProps = {
  countries: Country[];
};

export type AddFavoriteCountryReturnType = {
  payload: AddFavoriteCountryProps;
  type: ECountriesAction.ADD_FAVORITE;
};

export type RemoveFavoriteCountryReturnType = {
  payload: RemoveFavoriteCountryProps;
  type: ECountriesAction.REMOVE_FAVORITE;
};

export type FetchCountriesByRegionReturnType = {
  payload: FetchCountriesByRegionProps;
  type: ECountriesAction.FETCH_BY_REGION;
};

export type FetchCountriesByRegionSuccessReturnType = {
  payload: FetchCountriesByRegionSuccessProps;
  type: ECountriesAction.FETCH_BY_REGION_SUCCESS;
};

export type FetchCountriesByNameReturnType = {
  payload: FetchCountriesByNameProps;
  type: ECountriesAction.FETCH_BY_NAME;
};

export type FetchCountriesByNameSuccessReturnType = {
  payload: FetchCountriesByNameSuccessProps;
  type: ECountriesAction.FETCH_BY_NAME_SUCCESS;
};

export type CountriesAction =
  | FetchCountriesByRegionReturnType
  | FetchCountriesByRegionSuccessReturnType
  | AddFavoriteCountryReturnType
  | RemoveFavoriteCountryReturnType
  | FetchCountriesByNameReturnType
  | FetchCountriesByNameSuccessReturnType
  | {type: ECountriesAction.FETCH_ERROR};
