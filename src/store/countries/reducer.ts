import produce from 'immer';

import {defaultAsyncData} from '../consts';

import {CountriesAction, CountriesState, ECountriesAction} from './types';

const initialState: CountriesState = {
  countries: defaultAsyncData,
  favoritesCountries: [],
};

export const countriesReducer = (
  state = initialState,
  action: CountriesAction,
) =>
  produce(state, draft => {
    switch (action.type) {
      case ECountriesAction.ADD_FAVORITE:
        draft.favoritesCountries.push(action.payload.country);
        break;
      case ECountriesAction.REMOVE_FAVORITE:
        const removingCountryIndex = draft.favoritesCountries.findIndex(
          item => item?.ccn3 === action.payload.countryCode,
        );
        delete draft.favoritesCountries[removingCountryIndex];
        break;
      case ECountriesAction.FETCH_BY_REGION:
      case ECountriesAction.FETCH_BY_NAME:
        draft.countries.fetching = true;
        break;
      case ECountriesAction.FETCH_BY_REGION_SUCCESS:
      case ECountriesAction.FETCH_BY_NAME_SUCCESS:
        draft.countries.data = action.payload.countries;
        draft.countries.fetching = false;
        break;
      case ECountriesAction.FETCH_ERROR:
        draft.countries.data = null;
        draft.countries.fetching = false;
        break;
      default:
        return draft;
    }
  });
