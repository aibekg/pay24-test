import {AsyncStorage} from 'react-native';
import {combineReducers} from 'redux';
import {persistReducer} from 'redux-persist';

import {countriesReducer} from '@store/countries';
import {regionsReducer} from '@store/regions';

const countriesPersistConfig = {
  key: 'countries',
  storage: AsyncStorage,
  whitelist: ['favoritesCountries'],
};

export const rootReducer = combineReducers({
  countries: persistReducer(countriesPersistConfig, countriesReducer),
  regions: regionsReducer,
});

export type RootState = ReturnType<typeof rootReducer>;
