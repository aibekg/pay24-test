import {all} from 'redux-saga/effects';

import {countriesSaga} from './countries/effects';

export function* rootSaga() {
  yield all([countriesSaga()]);
}
