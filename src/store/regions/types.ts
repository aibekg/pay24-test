export type RegionsState = {
  activeRegion: Region;
};

export type RegionType = 'europe' | 'africa' | 'americas' | 'asia' | 'all';

export type Region = {
  id: number;
  label: RegionType;
};

export enum ERegionsAction {
  SET_ACTIVE_REGION = 'SET_ACTIVE_REGION',
}

export type SetActiveRegionProps = {
  region: Region;
};

export type SetActiveRegionReturnType = {
  payload: SetActiveRegionProps;
  type: ERegionsAction.SET_ACTIVE_REGION;
};

export type RegionsAction = SetActiveRegionReturnType;
