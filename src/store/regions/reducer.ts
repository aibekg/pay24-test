import {regions} from '@features/Home/Regions/Regions.mock';
import produce from 'immer';

import {ERegionsAction, RegionsAction, RegionsState} from './types';

const initialState: RegionsState = {
  activeRegion: regions[0],
};

export const regionsReducer = (
  state = initialState,
  {payload, type}: RegionsAction,
) =>
  produce(state, draft => {
    switch (type) {
      case ERegionsAction.SET_ACTIVE_REGION:
        draft.activeRegion = payload.region;
        break;
      default:
        return draft;
    }
  });
