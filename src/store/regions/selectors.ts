import {createSelector} from 'reselect';

import {RootState} from '@store/reducers';

import {RegionsState} from './types';

const selectedState = (state: RootState): RegionsState => state.regions;

export const selectActiveRegion = createSelector(
  selectedState,
  (state: RegionsState) => state.activeRegion,
);
