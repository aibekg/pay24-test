import {
  ERegionsAction,
  SetActiveRegionProps,
  SetActiveRegionReturnType,
} from './types';

export const setActiveRegion = (
  payload: SetActiveRegionProps,
): SetActiveRegionReturnType => ({
  type: ERegionsAction.SET_ACTIVE_REGION,
  payload,
});
