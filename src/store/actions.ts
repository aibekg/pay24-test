import * as CountriesActions from './countries/actions';
import * as RegionsActions from './regions/actions';

export default {
  ...CountriesActions,
  ...RegionsActions,
};
