export type AsyncData<T> = {
  fetching: boolean;
  data: T | null;
  error: string | null;
};

export const defaultAsyncData = {
  fetching: false,
  data: null,
  error: null,
};
