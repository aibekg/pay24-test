# Pay24 Mobile Test Project

Working on the Pay24 project requires some setup and guidelines you need to follow to work in sync with the team. This file will guide you through the setup and provide useful resources.

### ENV file

As you download the project you will not have a .env file included in every service.
You would need to ask other developers for these files

### Git rebase strategy

In this project we have moved to a rebase strategy. There are a lot of benefits to doing this, but it requires some knowhow and can cause problems if you do it incorectly.

> !!! look out with fixing problems on github, always to this locally !!!

Below are some links which explain the merging strategy, **always rebase interactively**! if however you do not feel confident about this, don't be afraid to ask the team for help.

- https://www.youtube.com/watch?v=f1wnYdLEpgI
- https://www.freecodecamp.org/news/an-introduction-to-git-merge-and-rebase-what-they-are-and-how-to-use-them-131b863785f/


## File Structure

`index.ts` should be used as a `Barrel` file.
A `barrel` is a way to rollup exports from several modules into a single convenient module.
The barrel itself is a module file that re-exports selected exports of other modules.

Example:
- CountryWidget/
  - CountryWidget.tsx       // contains the component and props type
  - CountryWidget.styles.ts // contains the styling for the component
  - index.tsx           // export component and props type

### src/components
All generic components that has no dependency on the application.
Can only contain internal state and should never use global state.

Examples: Button, Input, Icon,...

### src/features
App specific logic grouped per feature.
A feature can contain components, state, utils,...

Examples: Home, Country

### src/screens
Combining features and components to create a screen on the mobile phone.

Examples: HomeScreen

### src/navigation
stuff

### src/services
Contains interactions with side-effects like api calls, SDK's, ...etc

## Run
- Android: `cd android`, `./gradlew clean`, `cd ../`, `yarn android`
- IOS: `cd ios`, `pod install`, `cd ../`, `yarn ios`
- `yarn start --reset-cache`
